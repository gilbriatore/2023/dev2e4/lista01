package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio08 {

	public static void executar(Scanner leitor) {

		String s = "Ex 8. Escreva um programa que leia um número e mostre\r\n"
				+ "uma mensagem caso este número seja maior ou igual a\r\n" + "50, outra se ele for menor que 50.";

		System.out.println(s + "\n");

		int numero;
		System.out.println("Informe um número: ");
		numero = leitor.nextInt();

		if (numero >= 50) {
			System.out.printf("O número %d é maior ou igual a 50!", numero);
		} else {
			System.out.printf("O número %d é menor que 50!", numero);
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}