package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio05 {

	public static void executar(Scanner leitor) {

		String s = "Ex 5. Escreva um programa que leia valores nas variáveis A e B, e efetue a\r\n"
				+ "troca dos valores de forma que o valor da variável A passe a ser o valor da\r\n"
				+ "variável B e o valor da variável B passe a ser o valor da variável A.\r\n"
				+ "Apresentar uma mensagem com o valor original de cada variável e outra com os\r\n"
				+ "valores trocados.";

		System.out.println(s + "\n");

		int a, b, temp;
		System.out.println("Informe o valor de A: ");
		a = leitor.nextInt();
		System.out.println("Informa o valor de B: ");
		b = leitor.nextInt();

		System.out.println("----- Antes da troca -------");
		System.out.printf("O valor de A é: %d\n", a);
		System.out.printf("O valor de B é: %d\n\n", b);

		temp = a;
		a = b;
		b = temp;

		System.out.println("----- Depois da troca -------");
		System.out.printf("O valor de A é: %d\n", a);
		System.out.printf("O valor de B é: %d\n\n", b);

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}