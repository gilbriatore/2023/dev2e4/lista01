package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio02 {

	public static void executar(Scanner leitor) {

		String s = "Ex 2. Escreva um programa que leia dois números\r\n"
				+ "digitados pelo usuário e exiba o resultado da soma.";

		System.out.println(s + "\n");

		// números decimais
		double numero1, numero2, resultado;

		// informa o usuário
		System.out.println("Digite o primeiro número");

		// lo o valor digitado no tipo texto.
		String temp = leitor.nextLine();

		// converte o valor de texto para número.
		numero1 = Double.parseDouble(temp);

		// informa o usuário
		System.out.println("Digite o segundo número");

		// faz a leitura
		String temp2 = leitor.nextLine();

		// converte o a string para número
		numero2 = Double.parseDouble(temp2);

		resultado = numero1 + numero2;

		System.out.println("O resultado é: " + resultado);

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}