package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio07 {

	public static void executar(Scanner leitor) {

		String s = "Ex 7. Escreva um programa que leia um número e diga, através de uma mensagem,\r\n"
				+ "se este número está no intervalo entre 100 e 200. Caso o número esteja fora\r\n"
				+ "do intervalo o usuário também deverá ser informado.";

		System.out.println(s + "\n");

		int numero;

		System.out.println("Informe um número: ");
		numero = leitor.nextInt();

		if (numero >= 100 && numero <= 200) {
			System.out.printf("O número %d está entre o intervalo!", numero);
		} else {
			System.out.printf("O número %d está fora do intervalo!", numero);
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}