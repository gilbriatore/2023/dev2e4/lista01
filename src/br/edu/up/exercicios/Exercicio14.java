package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio14 {

	public static void executar(Scanner leitor) {

		String s = "Ex 14. A expressão an = a1 + (n - 1) * r é denominada termo geral da\r\n"
				+ "Progressão Aritmética (PA). Nesta fórmula, temos que an é o termo de ordem n\r\n"
				+ "(n-ésimo termo), r é a razão e a1 é o primeiro termo da Progressão\r\n"
				+ "Aritmética. Escreva um algoritmo que encontre o n-ésimo termo de uma\r\n"
				+ "progressão aritmética.\r\n" + "	  \r\n"
				+ "Exemplo: a1 = 10, n = 7, r = 3. Resultado: an = 28";

		System.out.println(s + "\n");

		int an, a1, n, r;

		System.out.println("Informe o primeiro termo: ");
		a1 = leitor.nextInt();

		System.out.println("Informe a razão: ");
		r = leitor.nextInt();

		System.out.println("Informe o n-ésimo termo desejado: ");
		n = leitor.nextInt();

		an = a1 + (n - 1) * r;

		System.out.printf("Resultado: %d", an);

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}