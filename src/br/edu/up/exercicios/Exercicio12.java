package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio12 {

	public static void executar(Scanner leitor) {

		String s = "Ex 12. Escreva um programa que receba o número do mês\r\n"
				+ "e mostre o mês correspondente. Valide mês inválido.";

		System.out.println(s + "\n");

		int mes;

		System.out.println("Informe o número do mês: ");
		mes = leitor.nextInt();

		System.out.print("O mês informado é: ");
		switch (mes) {
		case 1:
			System.out.println("JANEIRO");
			break;
		case 2:
			System.out.println("FEVEREIRO");
			break;
		case 3:
			System.out.println("MARÇO");
			break;
		case 4:
			System.out.println("ABRIL");
			break;
		case 5:
			System.out.println("MAIO");
			break;
		case 6:
			System.out.println("JUNHO");
			break;
		case 7:
			System.out.println("JULHO");
			break;
		case 8:
			System.out.println("AGOSTO");
			break;
		case 9:
			System.out.println("SETEMBRO");
			break;
		case 10:
			System.out.println("OUTUBRO");
			break;
		case 11:
			System.out.println("NOVEMBRO");
			break;
		case 12:
			System.out.println("DEZEMBRO");
			break;
		default:
			System.out.println("INVÁLIDO");
			break;
		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}