package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio13 {

	public static void executar(Scanner leitor) {

		String s = "Ex 13. Escreva um programa que leia valores REAIS nas variáveis A e B e o\r\n"
				+ "tipo de operador em outra variável do tipo CARACTERE. Imprima o resultado da\r\n"
				+ "operação de A por B se o operador aritmético for válido; caso contrário deve\r\n"
				+ "ser impresso uma mensagem de operador não definido. Tratar erro de divisão\r\n" + "por zero.";

		System.out.println(s + "\n");

		double a, b, resultado;
		char operador;

		System.out.println("Informe o número A: ");
		a = leitor.nextDouble();

		System.out.println("Informe o número B: ");
		b = leitor.nextDouble();

		// hack: necessário para avançar para a próxima linha.
		leitor.nextLine();

		System.out.println("Informe um operador (+, -, *, /): ");
		operador = leitor.nextLine().charAt(0);

		switch (operador) {
		case '+':
			resultado = a + b;
			System.out.printf("O resultado é: %.2f", resultado);
			break;
		case '-':
			resultado = a - b;
			System.out.printf("O resultado é: %.2f", resultado);
			break;
		case '*':
			resultado = a * b;
			System.out.printf("O resultado é: %.2f", resultado);
			break;
		case '/':
			if (b <= 0) {
				System.out.println("O divisor deve ser inteiro positivo!");
			} else {
				resultado = a / b;
				System.out.printf("O resultado é: %.2f", resultado);
			}
			break;
		default:
			System.out.println("Operador inválido!");
			break;
		}

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}