package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio18 {

	public static void executar(Scanner leitor) {

		String s = "Ex 18. Elabore um algoritmo que receba três notas de um aluno e retorne a sua\r\n"
				+ "média harmônica. Fórmula: 3 / ((1 / nota1) + (1 / nota2) + (1 / nota3))\r\n"
				+ "Exemplo: nota1 = 10.0, nota2 = 5.5, nota3 = 8.0. Média: 7.37";

		System.out.println(s + "\n");

		double media, nota1, nota2, nota3;

		System.out.println("Informe a primeira nota: ");
		nota1 = leitor.nextDouble();

		System.out.println("Informe a segunda nota: ");
		nota2 = leitor.nextDouble();

		System.out.println("Informe a terceira nota: ");
		nota3 = leitor.nextDouble();

		media = 3 / ((1 / nota1) + (1 / nota2) + (1 / nota3));

		System.out.printf("A média harmônica é: %.2f", media);

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}