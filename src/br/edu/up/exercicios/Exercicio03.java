package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio03 {

	public static void executar(Scanner leitor) {

		String s = "Ex 3. Escreva um programa que leia os valores de dois números inteiros\r\n"
				+ "distintos nas variáveis A e B e informe qual deles é o maior. Caso os números sejam\r\n"
				+ "iguais informar ao usuário que a sequência de números informados é inválida.";

		System.out.println(s + "\n");

		int a, b;

		System.out.println("Informe o número A: ");
		a = leitor.nextInt();

		System.out.println("Informe o número B: ");
		b = leitor.nextInt();

		if (a > b) {
			System.out.println("O número A é maior do que B!");
		} else if (b > a) {
			System.out.println("O múmero B é maior do que A!");
		} else {
			System.out.println("Os números são inválidos!");
		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}