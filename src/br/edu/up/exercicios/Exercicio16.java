package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio16 {

	public static void executar(Scanner leitor) {

		String s = "Ex 16. Elabore um programa que receba três notas de um\r\n"
				+ "aluno e retorne a sua média aritmética.\r\n\r\n"
				+ "Exemplo: nota1 = 10.0, nota2 = 5.5, nota3 = 8.0. Média: 7.83";

		System.out.println(s + "\n");

		double nota1, nota2, nota3, media;

		System.out.println("Informe a primeira nota: ");
		nota1 = leitor.nextDouble();

		System.out.println("Informe a segunda nota: ");
		nota2 = leitor.nextDouble();

		System.out.println("Informe a terceira nota: ");
		nota3 = leitor.nextDouble();

		media = (nota1 + nota2 + nota3) / 3;

		System.out.printf("A média aritmética é: %.2f", media);

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}