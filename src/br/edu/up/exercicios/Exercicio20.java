package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio20 {

	public static void executar(Scanner leitor) {

		String s = "Ex 20. Elabore um algoritmo que calcule a quantidade de litros\r\n"
				 + "de combustível gasto em uma viagem, utilizando um automóvel que\r\n"
				 + "faz 12km e considerando que são fornecidos o tempo em hora\r\n"
				 + "e a velocidade média da viagem.";

		System.out.println(s + "\n");

		double consumo, tempo, velocidadeMedia, distancia;

		System.out.println("Informe o tempo de viagem em horas: ");
		tempo = leitor.nextDouble();

		System.out.println("Informe a velocidade média: ");
		velocidadeMedia = leitor.nextDouble();

		// velocidade m�dia = dist�ncia(km) / tempo(h)
		distancia = velocidadeMedia * tempo;
		consumo = distancia / 12;

		System.out.printf("A distância percorrida foi de %.2fkm\n", distancia);
		System.out.printf("O consumo em litros é %.2f", consumo);

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}