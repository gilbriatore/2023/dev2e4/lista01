package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio01 {

	public static void executar(Scanner leitor) {

		String s = "Ex 1. Escreva um algoritmo que leia um número digitado pelo usuário e mostre\r\n"
				+ "a mensagem \"Número maior do que 10!\", caso este número seja maior, ou \"Número\r\n"
				+ "menor ou igual a 10!\", caso este número seja menor ou igual.";

		System.out.println(s + "\n");

		int numero;

		System.out.println("Digite um número:");

		// Faz a leitura do valor digitado
		String txt = leitor.nextLine();

		// Converte de texto para inteiro
		numero = Integer.parseInt(txt);

		// Verifica se o número é maior, menor ou igual a 10...
		if (numero > 10) {
			// Mostrar mensagem...
			System.out.println("Número maior do que 10!");
		} else {
			// Mostrar mensagem...
			System.out.println("Número menor ou igual a 10!");
		}

		System.out.println();
		System.out.println("Pressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}

	}
}