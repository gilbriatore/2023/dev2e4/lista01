package br.edu.up.exercicios;

import java.io.IOException;
import java.util.Scanner;

public class Exercicio06 {

	public static void executar(Scanner leitor) {

		String s = "Ex 6. Ler uma temperatura em graus Celsius e\r\n"
				+ "apresentá-la convertida em graus Fahrenheit. A\r\n"
				+ "fórmula de conversão é: F = (9 * C + 160) / 5";

		System.out.println(s + "\n");

		double celsius, fahrenheit;

		System.out.println("Informe a temperatura em Celsius: ");
		celsius = leitor.nextDouble();

		fahrenheit = (9 * celsius + 160) / 5;

		System.out.printf("Temperatura em Fahrenheit: %.0f", fahrenheit);

		System.out.println();
		System.out.println("\nPressione ENTER para voltar...");
		try {
			System.in.read();
		} catch (IOException e) {
		}
	}
}