package br.edu.up.app;

import br.edu.up.exercicios.*;
import java.util.Scanner;

public class Menu {

	public static void mostrar() {

		//Será utilizado apenas um Scanne
		//em todos os exercícios.
		Scanner leitor = new Scanner(System.in);
		
		int opcao = 0;

		do {
			System.out.println("Digite uma das opções:");
			int qtdeDeExercicios = 20;
			for (int i = 1; i <= qtdeDeExercicios; i++) {
				System.out.println("> Exercício (" + i + ")");
			}
			System.out.println("> Sair (0)");
			System.out.println();
			
			opcao = leitor.nextInt();

			//Avança o leitor para próxima linha...
			leitor.nextLine();

			System.out.println();

			switch (opcao) {
			case 1:
				Exercicio01.executar(leitor);
				break;
			case 2:
				Exercicio02.executar(leitor);
				break;
			case 3:
				Exercicio03.executar(leitor);
				break;
			case 4:
				Exercicio04.executar(leitor);
				break;
			case 5:
				Exercicio05.executar(leitor);
				break;
			case 6:
				Exercicio06.executar(leitor);
				break;
			case 7:
				Exercicio07.executar(leitor);
				break;
			case 8:
				Exercicio08.executar(leitor);
				break;
			case 9:
				Exercicio09.executar(leitor);
				break;
			case 10:
				Exercicio10.executar(leitor);
				break;
			case 11:
				Exercicio11.executar(leitor);
				break;
			case 12:
				Exercicio12.executar(leitor);
				break;
			case 13:
				Exercicio13.executar(leitor);
				break;
			case 14:
				Exercicio14.executar(leitor);
				break;
			case 15:
				Exercicio15.executar(leitor);
				break;
			case 16:
				Exercicio16.executar(leitor);
				break;
			case 17:
				Exercicio17.executar(leitor);
				break;
			case 18:
				Exercicio18.executar(leitor);
				break;
			case 19:
				Exercicio19.executar(leitor);
				break;
			case 20:
				Exercicio20.executar(leitor);
				break;
			}
			System.out.println();
		} while (opcao != 0);

		leitor.close();
		System.out.println("Programa encerrado!");
	}
}